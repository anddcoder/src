<?php

namespace Font\PageBundle\Entity;
use Font\PageBundle\Entity\User;
use Font\PageBundle\Entity\Imputation;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Finder\Comparator\DateComparator;
use Doctrine\ORM\Mapping as ORM;

/**
 * Projet
 *
 * @ORM\Table(name="projet")
 * @ORM\Entity(repositoryClass="Font\PageBundle\Repository\ProjetRepository")
 */
class Projet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    protected $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="date")
     */
    protected $dateDebut;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="date")
     */
    protected $dateFin;
       
    public function validate(ExecutionContextInterface $context) {


        $dO = $this->dateDebut;
        $dF = $this->dateFin;

        $dateComparator = new DateComparator($dF->format('d-M-y'));
        $dateComparator->setOperator(">=");
       //($dO->format("d-M-y"))>=($dF->format("d-M-y"));
        if (($dateComparator->test($dO->format('U')))) {
            $propertyPath = $context->getPropertyPath() . '.dateFin';

            //$context->setPropertyPath($propertyPath);
            $context->buildViolation("la date de fin doit être supérieur   à la date de début")
                ->atPath($propertyPath)
                ->addViolation();

            //$context->addViolation('Votre annonce doit être valide pendant au moins un mois', array(), null);
//            $context->addViolationAt(
//                    'dateOuverture', 'Erreur! la date d\'ouverture est supérieure à la date de fermeture', array(), null
//            );
        }
       
    }

    /**
     * @var string
     *
     * @ORM\Column(name="domaineApplication", type="string", length=255)
     */
    protected $domaineApplication;
    /**
    * @ORM\ManyToOne(targetEntity="User",inversedBy="projets")
    * @ORM\JoinColumn(name="user_id",referencedColumnName="id")
    */
    protected $user;

    /**
    * @ORM\OneToMany(targetEntity="Imputation",mappedBy="projet")
    */
    protected $imputations;

     /**
     * @ORM\ManyToMany(targetEntity="User",cascade={"persist"})
     *  @ORM\JoinTable(name="membresProjet",
     *      joinColumns={@ORM\JoinColumn(name="Projet_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    protected $membres;

    /**
     *
     * @ORM\Column(name="ferme", type="boolean")
     */
    protected $ferme;




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->membres = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ferme=false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Projet
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Projet
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Projet
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Projet
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set domaineApplication
     *
     * @param string $domaineApplication
     * @return Projet
     */
    public function setDomaineApplication($domaineApplication)
    {
        $this->domaineApplication = $domaineApplication;

        return $this;
    }

    /**
     * Get domaineApplication
     *
     * @return string 
     */
    public function getDomaineApplication()
    {
        return $this->domaineApplication;
    }

    /**
     * Set user
     *
     * @param \Font\PageBundle\Entity\User $user
     * @return Projet
     */
    public function setUser(\Font\PageBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Font\PageBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add membres
     *
     * @param \Font\PageBundle\Entity\User $membres
     * @return Projet
     */
    public function addMembre(\Font\PageBundle\Entity\User $membres)
    {
        $this->membres[] = $membres;

        return $this;
    }

    /**
     * Remove membres
     *
     * @param \Font\PageBundle\Entity\User $membres
     */
    public function removeMembre(\Font\PageBundle\Entity\User $membres)
    {
        $this->membres->removeElement($membres);
    }

    /**
     * Get membres
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMembres()
    {
        return $this->membres;
    }

    /**
     * Add imputations
     *
     * @param \Font\PageBundle\Entity\Imputation $imputations
     * @return Projet
     */
    public function addImputation(\Font\PageBundle\Entity\Imputation $imputations)
    {
        $this->imputations[] = $imputations;

        return $this;
    }

    /**
     * Remove imputations
     *
     * @param \Font\PageBundle\Entity\Imputation $imputations
     */
    public function removeImputation(\Font\PageBundle\Entity\Imputation $imputations)
    {
        $this->imputations->removeElement($imputations);
    }

    /**
     * Get imputations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImputations()
    {
        return $this->imputations;
    }

   

    /**
     * Set ferme
     *
     * @param boolean $ferme
     * @return Projet
     */
    public function setFerme($ferme)
    {
        $this->ferme = $ferme;

        return $this;
    }

    /**
     * Get ferme
     *
     * @return boolean 
     */
    public function getFerme()
    {
        return $this->ferme;
    }
}
