<?php

namespace Font\PageBundle\Entity;

use Font\PageBundle\Entity\User;
use Font\PageBundle\Entity\Projet;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Imputation
 *
 * @ORM\Table(name="imputation")
 * @ORM\Entity(repositoryClass="Font\PageBundle\Repository\ImputationRepository")
 */
class Imputation {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="duree", type="bigint")
     */
    private $duree;

   public function validate2(ExecutionContextInterface $context) {


       
     
        if ($this->duree>8) {
            $propertyPath = $context->getPropertyPath() . '.dateFin';

            //$context->setPropertyPath($propertyPath);
            $context->buildViolation("vous ne pouvez pas imputer plus de 8h par jour!")
                ->atPath($propertyPath)

                ->addViolation();

            //$context->addViolation('Votre annonce doit être valide pendant au moins un mois', array(), null);
//            $context->addViolationAt(
//                    'dateOuverture', 'Erreur! la date d\'ouverture est supérieure à la date de fermeture', array(), null
//            );
        }
       
    }
    /**
     * @ORM\ManyToOne(targetEntity="Projet",inversedBy="imputations",cascade={"persist"})
     * @ORM\JoinColumn(name="projet_id",referencedColumnName="id")
     */
    protected $projet;

    /**
     * @ORM\ManyToOne(targetEntity="User",cascade={"persist"})
     * @ORM\JoinColumn(name="membre_id", referencedColumnName="id")
     */
    protected $membre;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Imputation
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set duree
     *
     * @param integer $duree
     * @return Imputation
     */
    public function setDuree($duree) {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return integer 
     */
    public function getDuree() {
        return $this->duree;
    }

    /**
     * Set projet
     *
     * @param \Font\PageBundle\Entity\Projet $projet
     * @return Imputation
     */
    public function setProjet(\Font\PageBundle\Entity\Projet $projet) {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \Font\PageBundle\Entity\Projet 
     */
    public function getProjet() {
        return $this->projet;
    }

    /**
     * Set membre
     *
     * @param \Font\PageBundle\Entity\User $membre
     * @return Imputation
     */
    public function setMembre(\Font\PageBundle\Entity\User $membre) {
        $this->membre = $membre;

        return $this;
    }

    /**
     * Get membre
     *
     * @return \Font\PageBundle\Entity\User 
     */
    public function getMembre() {
        return $this->membre;
    }

}
