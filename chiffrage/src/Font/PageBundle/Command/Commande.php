<?php

namespace Font\PageBundle\Command;

 
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use FOS\UserBundle\Model\User;
use FOS\UserBundle\Command\CreateUserCommand as BaseCommand;
 
class CreateUserCommand extends BaseCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        parent::configure();
        $this
            ->setName('acme:user:create')
            ->getDefinition()->addArguments(array(
                new InputArgument('nom', InputArgument::REQUIRED, 'Nom'),
                new InputArgument('prenom', InputArgument::REQUIRED, 'Prenom'),
                new InputArgument('fonction', InputArgument::REQUIRED, 'Fonction')
            ))
        ;
        $this->setHelp(<<<EOT
// L'aide qui va bien
EOT
            );
    }
 
    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username   = $input->getArgument('username');
        $email      = $input->getArgument('email');
        $password   = $input->getArgument('password');
        $nom  = $input->getArgument('nom');
        $prenom   = $input->getArgument('prenom');
        $fonction   = $input->getArgument('fonction');
        $inactive   = $input->getOption('inactive');
        $superadmin = $input->getOption('super-admin');
 
        /** @var \FOS\UserBundle\Model\UserManager $user_manager */
        $user_manager = $this->getContainer()->get('fos_user.user_manager');
 
        /** @var \Acme\AcmeUserBundle\Entity\User $user */
        $user = $user_manager->createUser();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPlainPassword($password);
        $user->setEnabled((Boolean) !$inactive);
        $user->setSuperAdmin((Boolean) $superadmin);
        $user->setNom($nom);
        $user->setPrenom($prenom);
        $user->setFonction($fonction);
 
        $user_manager->updateUser($user);
 
        $output->writeln(sprintf('Created user <comment>%s</comment>', $username));
    }
 
    /**
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        parent::interact($input, $output);
        if (!$input->getArgument('nom')) {
            $nom = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose a nom:',
                function($nom) {
                    if (empty($nom)) {
                        throw new \Exception('Firstname can not be empty');
                    }
 
                    return $nom;
                }
            );
            $input->setArgument('nom', $nom);
        }
        if (!$input->getArgument('prenom')) {
            $prenom = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose a prenom:',
                function($prenom) {
                    if (empty($prenom)) {
                        throw new \Exception('prenom can not be empty');
                    }
 
                    return $prenom;
                }
            );
            $input->setArgument('prenom', $prenom);
        }
        if (!$input->getArgument('fonction')) {
            $fonction = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please choose a fonction:',
                function($fonction) {
                    if (empty($fonction)) {
                        throw new \Exception('prenom can not be empty');
                    }
 
                    return $fonction;
                }
            );
            $input->setArgument('fonction', $fonction);
        }
    }
}