<?php

namespace Font\PageBundle\Command;

use Symfony\Bridge\PhpUnit\TextUI\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use FOS\UserBundle\Command\CreateUserCommand as BaseCommand;

class CreateUserCommand extends BaseCommand {

    /**
     * @see Command
     */
    protected function configure() {
        parent::configure();

        $this->getName('Font:user:create');
        $this->getDefinition()->addArguments(array(
            new InputArgument('nom', InputArgument::REQUIRED, 'nom'),
            new InputArgument('prenom', InputArgument::REQUIRED, 'prenom'),
            new InputArgument('fonction', InputArgument::REQUIRED, 'Fonction')
                )
        );
        $this->setHelp(<<<EOT
The <info>fos:user:create</info> command creates a user:
 
  <info>php app/console fos:user:create matthieu</info>
 
This interactive shell will ask you for an email and then a password.
 
You can alternatively specify the email and password as the second and third arguments:
 
  <info>php app/console fos:user:create matthieu matthieu@example.com mypassword</info>
 
You can create a super admin via the super-admin flag:
 
  <info>php app/console fos:user:create admin --super-admin</info>
 
You can create an inactive user (will not be able to log in):
 
  <info>php app/console fos:user:create thibault --inactive</info>
EOT
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $username = $input->getArgument('username');
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $prenom = $input->getArgument('prenom');
        $nom = $input->getArgument('nom');
        $fonction = $input->getArgument('fonction');

        $inactive = $input->getOption('inactive');
        $superadmin = $input->getOption('super-admin');

        /** @var \FOS\UserBundle\Model\UserManager $user_manager */
        $user_manager = $this->getContainer()->get('fos_user.user_manager');

        /** @var \Font\PageBundle\Entity\User $user */
        $user = $user_manager->createUser();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPlainPassword($password);
        $user->setEnabled((Boolean) !$inactive);
        $user->setSuperAdmin((Boolean) $superadmin);

        $user->setPrenom($prenom);
        $user->setNom($nom);
        $user->setFonction($fonction);
        $user_manager->updateUser($user);

        $output->writeln(sprintf('Created user <comment>%s</comment>', $username));
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output) {
        parent::interact($input, $output);
        if (!$input->getArgument('nom')) {
            $nom = $this->getHelper('dialog')->askAndValidate(
                    $output, 'Please choose nom:', function($nom) {
                if (empty($nom)) {
                    throw new \Exception('Nom can not be empty');
                }
                return $nom;
            }
            );
            $input->setArgument('nom', $nom);
        }
        if (!$input->getArgument('prenom')) {
            $prenom = $this->getHelper('dialog')->askAndValidate(
                    $output, 'Please choose prenom:', function($prenom) {
                if (empty($prenom)) {
                    throw new \Exception('Prenom can not be empty');
                }
                return $prenom;
            }
            );
            $input->setArgument('prenom', $prenom);
        }
        if (!$input->getArgument('fonction')) {
            $fonction = $this->getHelper('dialog')->askAndValidate(
                    $output, 'Please choose a fonction:', function($fonction) {
                if (empty($fonction)) {
                    throw new \Exception('prenom can not be empty');
                }

                return $fonction;
            }
            );
            $input->setArgument('fonction', $fonction);
        }
    }

}
