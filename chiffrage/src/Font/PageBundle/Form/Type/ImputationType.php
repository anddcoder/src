<?php

namespace Font\PageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ImputationType extends AbstractType


{
private $securityContext;
private $test;

    public function __construct(SecurityContext $securityContext,$test = NULL)
    {
        $this->securityContext = $securityContext;
        $this->test = $test;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
               
                ->add('duree')
                ->add('projet','Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                            'class' => 'FontPageBundle:Projet',
                            "choices" => $this->test,
                            "multiple" => false,
                            "expanded" => false,
                            'placeholder'=>'Selectionner un projet',
                            'choice_label' => function ($Projet) {
                               
                               return $Projet->getNom(); 
                                
                            }
                ))
                ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Font\PageBundle\Entity\Imputation'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'imputation_form';
    }

    public function getName(){

        return 'imputation_form';
    }


}
