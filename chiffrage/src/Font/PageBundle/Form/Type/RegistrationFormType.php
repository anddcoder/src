<?php

namespace Font\PageBundle\Form\Type;



use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
 
class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
 
        $builder->add('nom');
        $builder->add('prenom');
        $builder->add('fonction')
                ->add('roles', 'collection',
                    array('label'=>'Roles', 'type' => 'choice', 'options' =>
                            array( 'choices' => array('ROLE_MEMBRE' => 'Membre Equipe Projet', 'ROLE_ADMIN' => 'admin','ROLE_CHEF_PROJET'=>'Chef Projet') ) ))
;
        

    }
 
 
    public function getName() {
        return 'acme_user_registration';
    }

}
