<?php

namespace Font\PageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProjetType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                
            ->add('nom')
            ->add('description','textarea',array('attr'=>array('rows'=>6)))
            ->add('domaineApplication',new ChoiceType (), array(
                    'choices' => array('Informatique' => 'Informatique', 'Construction Infrastructure' => 'Construction Infrastructure'),
                    'choices_as_values' => true,
                    'placeholder'=>'Choisissez un domaine',
                ))
            ->add('dateDebut','date',array(
                 'format' => 'd-M-y',)
            )
            ->add('dateFin', 'date', array(
                 'format' => 'd-M-y',)
            )
            
           
            
            
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Font\PageBundle\Entity\Projet'
        ));
    }
    public function getName() {
        return 'projet_form';
    }
}
