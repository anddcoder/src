<?php

namespace Font\PageBundle\Form\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use Font\PageBundle\Entity\Projet;
use Font\PageBundle\Entity\User;
use FOS\UserBundle\Doctrine\UserManager;
class FormHandler {
/**
 *
 * @var type 
 */
    protected $form;
    protected $request;
    protected $em;
    protected $security;
    /**
 * 
 * @param Form $form
 * @param Request $request
 */
    public function __construct(Form $form, Request $request,EntityManager $em,SecurityContext $security) {
        $this->form = $form;
        $this->request = $request;
        $this->em=$em;
        $this->security=$security;
    }
/**
 * 
 * @return boolean
 */
    public function process() {
        $this->form->handleRequest($this->request);

        if ($this->request->getMethod() == 'POST' && $this->form->isValid()) {
            $this->OneSuccess();
            return true;
        }
        return false;
    }
/**
 * 
 * @return form
 */
    public function getForm() {
        return $this->form;
    }

    protected function OneSuccess() {
        $projet=$this->form->getData();

        $user=new User();
       $userManager = $this->get('fos_user.user_manager');
        $users = $userManager->findUsers();

        $ids = $this->request->get('checkbox');

        foreach ($ids as $id) {
            $user=$users->find($id);
            $projet->addMembre($user);

        }

        

    
        $projet->setUser($this->security->getToken()->getUser());
        $this->em->persist($projet);
        $this->em->flush();
    }

}
