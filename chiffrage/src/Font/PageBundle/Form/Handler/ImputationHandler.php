<?php
namespace Font\PageBundle\Form\Handler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;
use Font\PageBundle\Entity\Projet;
use Font\PageBundle\Form\Type\ImputationType;


class ImputationHandler{

/**
 *
 * @var type 
 */
	protected $form;
    protected $request;
    protected $em;
     protected $security;

   /**
 * 
 * @param Form $form
 * @param Request $request
 *
 */
   public function __construct(Form $form, Request $request,EntityManager $em,SecurityContext $security) {
        $this->form = $form;
        $this->request = $request;
        $this->em=$em;
        $this->security=$security;
     
    }
 /**
 * 
 * @return boolean
 */
    public function process() {
        $this->form->handleRequest($this->request);

        if ($this->request->getMethod() == 'POST' && $this->form->isValid()) {
            $this->OneSuccess();
            return true;
        }
        return false;
    }

    /**
 * 
 * @return form
 */
    public function getForm(SecurityContext $security) {
    
        return $this->form;
    }

    protected function OneSuccess() {

   
        
       
       

        
    }


}