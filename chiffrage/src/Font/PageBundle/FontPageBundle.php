<?php

namespace Font\PageBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FontPageBundle extends Bundle
{
     public function getParent()
    {
        return 'FOSUserBundle';
    }
}
