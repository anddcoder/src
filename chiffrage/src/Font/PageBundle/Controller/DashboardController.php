<?php

namespace Font\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Font\PageBundle\Form\Type\ImputationType;
use Font\PageBundle\Entity\Projet;
use Font\PageBundle\Entity\User;
use Font\PageBundle\Entity\Imputation;


class DashboardController extends Controller {

    public function indexAction(Request $request) {
        $rootPath = $this->get('router')->generate('font_page_homepage', array(), true);
        $referer = $request->server->get("HTTP_REFERER");

        if ($rootPath == $referer) {
            $bag = $this->get('session')->getFlashBag();
            $bag->set('login', 'vous étes bien connecté');
        }
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('FontPageBundle:Projet');
        $NoImputs = $repository->getNoImputtoDay($this->getUser(),$em);
      
        return $this->render('FontPageBundle:Dashboard:index.html.twig', array('ToDo' => $NoImputs));
    }

    //creation projet
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $projet = new Projet();


        $formHandler = $this->get('projet_handler');

        $userManager = $this->get('fos_user.user_manager');
        $users = $userManager->findUsers();
        $Users = array();

        foreach ($users as $user) {
            if (!$user->hasRole('ROLE_ADMIN') && !$user->hasRole('ROLE_CHEF_PROJET') && $user !== $this->getUser())
                array_push($Users, $user);
        }

        $formHandler->getForm()->handleRequest($request);



        if ($formHandler->getForm()->isValid()) {
            $projet = $formHandler->getForm()->getData();
            $user = new User();
            $ids = $this->getRequest()->request->get('checkbox');
            if ($ids) {
                foreach ($ids as $id) {
                    $user = $userManager->findUserBy(array('id' => $id));
                    $projet->addMembre($user);
                }
                $projet->setUser($this->getUser());
                $projet->addMembre($this->getUser());
                $em->persist($projet);
                $em->flush();
                $formHandler = $this->get('projet_handler');
                $request->getSession()->getFlashBag()->add('success', 'Opération de Creation Projet effectuée avec succés!');
                return $this->redirect($this->generateUrl('font_page_liste_projet'));
            } else {
                $request->getSession()->getFlashBag()->add('success', 'Veuillez mettre au moins un membre dans votre projet!');
            }
        }




        return $this->render('FontPageBundle:Dashboard:CreerProjet.html.twig', array('form' => $formHandler->getForm()->createView(), 'users' => $Users));
    }

    //liste des projets

    public function listprojetAction() {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('FontPageBundle:Projet');
        $Mes_projet = $repository->getMyProjet($this->getUser());
        $projet = $repository->getProjetMembre($this->getUser());
        //$Projects=$repository->findByUser($this->getUser());
        
       
        return $this->render('FontPageBundle:Dashboard:ListesProjet.html.twig', array('projet' => $Mes_projet, 'others_projets' => $projet));
    }
    public function detailProAction($idPro) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('FontPageBundle:Projet');
        $Projects=$repository->findById($idPro);
         $repository2 = $em->getRepository('FontPageBundle:Imputation');
        $results=array();
        $userManager = $this->get('fos_user.user_manager');
        
        
        foreach ($Projects as $projet) {
        
           $ids = $this->getRequest()->request->get('checkbox');
          if ($ids) {
                foreach ($ids as $id) {
                    $user = $userManager->findUserBy(array('id' => $id));
                    $projet->addMembre($user);
                }
                
                $projet->setUser($this->getUser());
                $em->persist($projet);
                
                $em->flush();
          }
           $membres=$projet->getMembres();
           $Users=array();
            $users = $userManager->findUsers();
        foreach ($users as $user) {
            if (!$user->hasRole('ROLE_ADMIN') && !$user->hasRole('ROLE_CHEF_PROJET')&& $user !== $this->getUser())
                array_push($Users, $user);
        }
        //   
            foreach ($membres as $user) {
                $result=array();
                
                
                unset($Users[array_search($user, $Users)]);
               $ImpMembre=$repository2->getAllImputs($user,$projet); 
               
               
               array_push($result, $ImpMembre);
               array_push($result, $user);
               array_push($results, $result);
            }
            
           
        }
        
       
       
         return $this->render('FontPageBundle:Dashboard:detailPro.html.twig', array('results' => $results,'projet'=>$Projects,'users'=>$Users));
    }
    // faire une imputation dans un projet
    public function imputerAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $imputer = new Imputation();
        $user = $this->get('security.context')->getToken()->getUser();

        $securityContext = $this->container->get('security.context');
        
        $repository = $em->getRepository('FontPageBundle:Projet');
        $NoImputs = $repository->getNoImputtoDay($this->getUser(),$em);
        
        $formHandler = $this->createForm(new ImputationType($securityContext,$NoImputs), $imputer);


        $formHandler->handleRequest($request);


        if ($formHandler->isSubmitted() && $formHandler->isValid()) {
            $imputer = $formHandler->getData();
            $repository = $em->getRepository('FontPageBundle:Imputation');
            $impu = $repository->getLastImputer($this->getUser(), $imputer->getProjet());

            if ($impu && date_format($impu[0]->getDate(), 'y-m-d') == date_format(new \Datetime(), 'y-m-d')) {

                $request->getSession()->getFlashBag()->add('Echec', 'Vous avez déjà imputé sur ce projet pour aujourdhui');
                //  $message='Vous avez déjà imputé sur ce projet pour aujourdhui';
            } else {

                $imputer->setMembre($user);
                $imputer->setDate(new \Datetime());
                $em->persist($imputer);
                $em->flush();
                $request->getSession()->getFlashBag()->add('success', 'Imputation de la journée effectuée avec succés!');
                return $this->redirect($this->generateUrl('font_page_liste_imputation'));
            }
        }

        return $this->render('FontPageBundle:Dashboard:imputer.html.twig', array('form' => $formHandler->createView(),'To_imput'=>$NoImputs));
    }

    public function ListeImputationAction() {

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('FontPageBundle:Imputation');
        $impu = $repository->getAllImpute($this->getUser());
        return $this->render('FontPageBundle:Dashboard:ListeImputations.html.twig', array('imputation' => $impu));
    }
    public function cloreProAction(Request $request){
        
     $id = $this->getRequest()->request->get('id');
     $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('FontPageBundle:Projet');
        $Project=$repository->findById($id);
        
        $Project[0]->setFerme(TRUE);
        $Project[0]->setDateFin(new \DateTime());
        $em->flush();
        
    return $this->redirect($this->generateUrl('font_page_detail_proget',array('idPro'=>$id)));
    }

}
