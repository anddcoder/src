<?php

namespace Font\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FontPageBundle:Default:index.html.twig');
    }
}
