<?php

namespace Font\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Font\PageBundle\Entity\User;
//use Sensio\Bundle\FrameworkExtraBuse AppBundle\Entity\Discipline;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class AdminController extends Controller {

    public function index1Action() {
        return $this->render('FontPageBundle:Admin:index.html.twig');
    }

    public function cUserAction() {

        return $this->render('FontPageBundle:Admin:creer_user.html.twig');
    }

   

    /**
     *@Route("/", name="user_new")
     * @Method("GET")
     */
    public function indexAction() {
        $datatable = $this->get('user_datatable');
        $datatable->buildDatatable();

        return $this->render('FontPageBundle:Admin:seeUsers.html.twig', array(
                    'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/results", name="user_results")
     */
    public function indexResultsAction() {
        $datatable = $this->get('user_datatable');
        $datatable->buildDatatable();

        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);

        return $query->getResponse();
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit", options={"expose"=true})
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id) {
        return $this->render('FontPageBundle:Admin:seeUsers2.html.twig');
    }

    /**
     * @Route("/{id}/show", name="user_show", options={"expose"=true})
     * @Method({"GET"})
     */
    public function showAction(Request $request, User $User) {
        return $this->indexAction();
    }

}
