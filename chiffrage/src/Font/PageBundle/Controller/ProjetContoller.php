<?php
namespace Font\PageBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Font\PageBundle\Form\Handler\FormHandler;
use Font\PageBundle\Form\Type\ProjetType;
use Symfony\Component\HttpFoundation\Request;

class ProjetController extends Controller {
    
    public function createAction(Request $request){
        $formHandler=new FormHandler($this->createForm(new ProjetType(),$request));
        
        
        return $this->render('FontPageBundle:Dashboard:Creer.html.twig',array('form'=>$formHandler->getForm()->createView()));
    }
    
}
